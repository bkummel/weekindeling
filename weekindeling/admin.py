from django.apps import apps
from django.contrib.admin import AdminSite
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django.urls import reverse, NoReverseMatch
from django.utils.text import capfirst

from activiteiten.admin import ActiviteitAdmin, HerhalendeActiviteitAdmin, FeestdagAdmin, \
    ThemaweekAdmin, SchoolvakantieAdmin, RuimteAdmin, LocatieAdmin, VerwarmingsgroepAdmin, MyModelAdmin, CategorieAdmin
from activiteiten.models import Activiteit, HerhalendeActiviteit, Feestdag, Themaweek, Schoolvakantie, \
    Ruimte, Locatie, Verwarmingsgroep, Categorie
from taken.admin import PersoonAdmin, TaakAdmin, TaakUitvoeringAdmin
from taken.models import Persoon, Taak, TaakUitvoering


class MyAdminSite(AdminSite):
    site_header = 'Beheer weekindeling'
    index_title = 'Activiteitenbeheer'
    index_template = 'admin/index.html'

    def _build_app_dict(self, request, label=None):
        """
        Build the app dictionary. The optional `label` parameter filters models
        of a specific app. This overridden version also groups ModelAdmins that subclass
        MyModelAdmin and have their `grouping` member set.
        """
        app_dict = {}

        if label:
            models = {
                m: m_a for m, m_a in self._registry.items()
                if m._meta.app_label == label
            }
        else:
            models = self._registry

        for model, model_admin in models.items():
            app_label = model._meta.app_label

            has_module_perms = model_admin.has_module_permission(request)
            if not has_module_perms:
                continue

            perms = model_admin.get_model_perms(request)

            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True not in perms.values():
                continue

            info = (app_label, model._meta.model_name)
            model_dict = {
                'name': capfirst(model._meta.verbose_name_plural),
                'object_name': model._meta.object_name,
                'perms': perms,
            }
            if perms.get('change'):
                try:
                    model_dict['admin_url'] = reverse('admin:%s_%s_changelist' % info, current_app=self.name)
                except NoReverseMatch:
                    pass
            if perms.get('add'):
                try:
                    model_dict['add_url'] = reverse('admin:%s_%s_add' % info, current_app=self.name)
                except NoReverseMatch:
                    pass

            group_label = app_label
            if isinstance(model_admin, MyModelAdmin):
                group = model_admin.grouping
                if group and len(group) > 0:
                    group_label = group

            if group_label in app_dict:
                app_dict[group_label]['models'].append(model_dict)
            else:
                app_dict[group_label] = {
                    'name': apps.get_app_config(app_label).verbose_name if app_label == group_label else group_label,
                    'app_label': app_label,
                    'group_label': group_label,
                    'app_url': reverse(
                        'admin:app_list',
                        kwargs={'app_label': app_label},
                        current_app=self.name,
                    ),
                    'has_module_perms': has_module_perms,
                    'models': [model_dict],
                }

        if label:
            # This extra code (compared to the django.contrib original) is needed to
            # re-unite the ModelAdmins that were split up in groups.
            my_apps = {k: v for k, v in app_dict.items() if v['app_label'] == label}
            models = []
            for app in my_apps:
                for m in app_dict[app]['models']:
                    models.append(m)
            my_app_dict = app_dict[label]
            my_app_dict['models'] = models
            return my_app_dict
        return app_dict


admin_site = MyAdminSite()

admin_site.register(Activiteit, ActiviteitAdmin)
admin_site.register(Categorie, CategorieAdmin)
admin_site.register(HerhalendeActiviteit, HerhalendeActiviteitAdmin)
# admin_site.register(BijzondereDag, BijzondereDagAdmin)
admin_site.register(Feestdag, FeestdagAdmin)
admin_site.register(Themaweek, ThemaweekAdmin)
admin_site.register(Schoolvakantie, SchoolvakantieAdmin)
admin_site.register(Ruimte, RuimteAdmin)
admin_site.register(Locatie, LocatieAdmin)
admin_site.register(Verwarmingsgroep, VerwarmingsgroepAdmin)

admin_site.register(Persoon, PersoonAdmin)
admin_site.register(Taak, TaakAdmin)
admin_site.register(TaakUitvoering, TaakUitvoeringAdmin)

admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)
