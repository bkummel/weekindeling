"""weekindeling URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.http import HttpResponse
from django.urls import path
from django.views.generic import RedirectView

from weekindeling.admin import admin_site

urlpatterns = [
    # login URLs
    url(r'^accounts/', include('django.contrib.auth.urls')),
    # admin URL
    url(r'^admin/', admin_site.urls),
    # front end sections
    url(r'^weekoverzicht/', include('activiteiten.weekoverzicht_urls')),
    url(r'^kalender/', include('activiteiten.kalender_urls')),
    url(r'^taak/', include('activiteiten.taak_urls')),
    # autocomplete URLs
    url(r'^taak-autocomplete/', include('taken.autocomplete_urls')),
    url(r'^activiteit-autocomplete/', include('activiteiten.autocomplete_urls')),
    # Smart select URLs
    url(r'^chaining/', include('smart_selects.urls')),

    # Next patterns are needed for the "forgot password" function of the admin site
    url(
        r'^admin/password_reset/$',
        auth_views.PasswordResetView.as_view(),
        name='admin_password_reset',
    ),
    url(
        r'^admin/password_reset/done/$',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    url(
        r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    url(
        r'^reset/done/$',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),

    # Add a robots.txt to prevent the app being indexed by search engines.
    url(
        r'^robots.txt$',
        lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type="text/plain"),
        name='robots'
    ),

    # Redirect to weekoverzicht from root
    url(r'^$', RedirectView.as_view(url='weekoverzicht/', permanent=False), name='index')

]
