from django.conf.urls import url

from taken.autocomplete_views import TaakAutocomplete
from .autocomplete_views import PersoonAutocomplete

urlpatterns = [
    url(
        r'^taak/$',
        TaakAutocomplete.as_view(),
        name='taak-autocomplete',
    ),
    url(
        r'^persoon/$',
        PersoonAutocomplete.as_view(),
        name='persoon-autocomplete',
    ),
]
