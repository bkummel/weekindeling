from dal import autocomplete
from django.db.models import Q

from .models import Persoon, Taak


class PersoonAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Persoon.objects.none()

        qs = Persoon.objects.all()

        taak = self.forwarded.get('taak', None)

        if taak:
            qs = qs.filter(taken__in=[taak])

        if self.q:
            qs = qs.filter(
                Q(voornaam__icontains=self.q) |
                Q(achternaam__icontains=self.q) |
                Q(tussenvoegsels__icontains=self.q)
            )

        return qs


class TaakAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Taak.objects.none()

        qs = Taak.objects.all()

        if self.q:
            qs = qs.filter(omschrijving__istartswith=self.q)

        return qs
