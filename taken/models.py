# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.contrib.auth.models import Group
from django.db import models
from django.template.defaultfilters import slugify

from smart_selects.db_fields import ChainedManyToManyField

from activiteiten.model_utils import DatumFilterManager

logger = logging.getLogger(__name__)


class Taak(models.Model):
    omschrijving = models.CharField(max_length=200, blank=False, null=False)
    indeling_tonen = models.BooleanField(default=False)
    groep = models.ForeignKey(to=Group, on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = "Taken"

    def personen(self):
        return self.personen_set.all()

    def slug(self):
        return slugify(self.omschrijving)

    def __str__(self):
        return self.omschrijving


class Persoon(models.Model):
    voornaam = models.CharField(max_length=200, blank=True, null=True)
    tussenvoegsels = models.CharField(max_length=100, blank=True, null=True)
    achternaam = models.CharField(max_length=200, blank=False, null=False)
    email_adres = models.EmailField(blank=False, null=False)
    taken = models.ManyToManyField(Taak, blank=True, related_name='personen_set')

    class Meta:
        verbose_name_plural = "Personen"
        ordering = ["achternaam", "voornaam"]

    def naam(self):
        return ' '.join(filter(None, (self.voornaam, self.tussenvoegsels, self.achternaam)))

    def __str__(self):
        return self.naam()


class TaakUitvoering(models.Model):
    taak = models.ForeignKey(Taak, on_delete=models.PROTECT, blank=False, null=False)
    datum = models.DateField()
    uitvoerenden = ChainedManyToManyField(
        Persoon,
        chained_field="taak",
        chained_model_field="taken",
    )
    filter_by = DatumFilterManager()
    objects = models.Manager()

    class Meta:
        verbose_name_plural = "Taak uitvoeringen"

    def wie(self):
        return ', '.join(map(lambda p: p.__str__(), self.uitvoerenden.all()))
