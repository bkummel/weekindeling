# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from activiteiten.admin import MyModelAdmin
from taken.forms import PersoonForm, TaakUitvoeringForm, PersoonDetailInlineForm
from .models import Persoon, Taak, TaakUitvoering


class PersoonAdmin(MyModelAdmin):
    list_display = ('voornaam', 'tussenvoegsels', 'achternaam')
    form = PersoonForm
    search_fields = ['voornaam', 'tussenvoegsels', 'achternaam']


class PersoonDetailInline(admin.TabularInline):
    model = Taak.personen_set.through
    form = PersoonDetailInlineForm


class TaakAdmin(MyModelAdmin):
    list_display = ('omschrijving', 'indeling_tonen', 'groep')
    inlines = [PersoonDetailInline, ]
    search_fields = ['omschrijving']


class TaakUitvoeringAdmin(MyModelAdmin):
    list_filter = ('taak', 'datum', 'uitvoerenden')
    list_display = ('taak', 'datum', 'wie')
    form = TaakUitvoeringForm
    search_fields = ['taak__omschrijving']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Limit choices for 'taak' field to only taak options that match your auth. groups."""
        if db_field.name == 'taak':
            if not request.user.is_superuser:
                kwargs["queryset"] = Taak.objects.filter(
                    groep__in=request.user.groups.all())
        return super(TaakUitvoeringAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super(TaakUitvoeringAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(taak__groep__in=request.user.groups.all())
