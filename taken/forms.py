# coding: utf-8

from dal import autocomplete

from taken.fixed_model_form import FixedModelForm
from taken.models import Persoon, TaakUitvoering


class PersoonDetailInlineForm(FixedModelForm):
    class Meta:
        model = Persoon
        fields = ('__all__')
        widgets = {
            'persoon': autocomplete.ModelSelect2(url='persoon-autocomplete')
        }
        help_texts = {
            'persoon': 'Begin te typen en selecteer een persoon uit het lijstje.'
        }


class PersoonForm(FixedModelForm):
    class Meta:
        model = Persoon
        fields = ('__all__')
        widgets = {
            'taken': autocomplete.ModelSelect2Multiple(url='taak-autocomplete')
        }


class TaakUitvoeringForm(FixedModelForm):
    class Meta:
        model = TaakUitvoering
        fields = ('__all__')
        widgets = {
            'uitvoerenden': autocomplete.ModelSelect2Multiple(url='persoon-autocomplete', forward=['taak'])
        }
        help_texts = {
            'uitvoerenden': 'Begin te typen en selecteer een persoon uit het lijstje.'
        }

