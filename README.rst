============
Weekindeling
============

This project creates a system to plan activities in rooms and use resources. It is designed for,
and currently in use by a church to plan weekly activities.

Features
--------

Currently, the following features are implemented:

Admin interface:
''''''''''''''''

* Creating activities
* Creating recurring activities
* Assigning activities to rooms
* Defining tasks
* Assigning tasks to people
* Defining holidays and school vacations

Front end
'''''''''

* Weekly view
* Year view with date selection
* Tasks view


Roadmap
-------

The following features are on the roadmap:

* Adding categories and/or tags to activities in order to filter on them.
* Create iCal-endpoints based on filters for integration with calendar applications, like Google Calendar,
  Apple iCal, etc.
* Prevent double booking of rooms
* Create front end form to request activity/booking
* Internationalisation (currently all texts are in Dutch...)
* Further improving responsiveness of the front end
* ...


Project
-------

The project is started as a tailored solution for a church. I would like to make it more versatile, so
other organisations can use the system as well. Ideas and contributions are welcome!