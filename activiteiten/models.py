# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import logging
from calendar import monthrange
from datetime import date

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models
from smart_selects.db_fields import ChainedManyToManyField, ChainedForeignKey

from activiteiten.model_utils import DatumFilterManager, StartEindDatumFilterManager, BijzonderedagManager, \
    LowerCaseCharField
from taken.models import Taak, Persoon

logger = logging.getLogger(__name__)


class Locatie(models.Model):
    naam = models.CharField(max_length=200)
    tonen_bij_ruimte = models.BooleanField(default=True, help_text='Als je deze optie aanzet, wordt voor deze locatie '
                                                                   'de locatie altijd getoond op overzichten, je '
                                                                   'krijgt dan bijvoorbeeld "Grote zaal, Amersfoort". '
                                                                   'Als je deze optie uitzet, wordt het zelfde '
                                                                   'voorbeeld: "Grote zaal", zonder locatie erachter.')

    def __str__(self):
        return self.naam


class Verwarmingsgroep(models.Model):
    naam = models.CharField(max_length=200)
    locatie = models.ForeignKey(Locatie, on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        verbose_name_plural = "verwarmingsgroepen"

    def __str__(self):
        return self.naam


class Ruimte(models.Model):
    naam = models.CharField(max_length=200)
    locatie = models.ForeignKey(Locatie, on_delete=models.CASCADE, blank=False, null=False)
    capaciteit = models.PositiveIntegerField(blank=True, null=True)
    opmerkingen = models.CharField(max_length=500, blank=True, null=True)
    verwarmingsgroep = ChainedForeignKey(
        Verwarmingsgroep,
        chained_field="locatie",
        chained_model_field="locatie",
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    def __str__(self):
        arguments = []
        if self.capaciteit:
            arguments.append('%d personen' % self.capaciteit)
        if self.opmerkingen:
            arguments.append(self.opmerkingen)
        if self.verwarmingsgroep:
            arguments.append(self.verwarmingsgroep.naam)

        if len(arguments) > 0:
            return '%s (%s)' % (self.naam, ", ".join(arguments))
        else:
            return self.naam


def months_from(aantal, datum):
    dagen = 0
    jaar = datum.year
    maand = datum.month
    for m in range(aantal):
        dagen += monthrange(jaar, maand)[1]
        maand = maand + 1
        if maand == 13:
            maand = 1
            jaar = jaar + 1

    return datum + datetime.timedelta(days=dagen)


def do_normalize_month_dates(start, eind):
    return (
        datetime.date(start.year, start.month, 1),
        datetime.date(eind.year, eind.month, monthrange(eind.year, eind.month)[1]))


def months_between(startdatum, einddatum):
    (start, eind) = do_normalize_month_dates(startdatum, einddatum)
    eind = eind + datetime.timedelta(days=1)
    jaren = eind.year - start.year
    if jaren > 0:
        return (12 - start.month) + eind.month + ((jaren - 1) * 12)
    else:
        return eind.month - start.month


def normalize_ymd(year, month, day):
    year = year + ((month - 1) // 12)
    if month > 12:
        month = month % 12
    (minimum, maximum) = monthrange(year, month)
    day = min(day, maximum)
    return year, month, day


def nth_day_of_month(year, month, day):
    (year, month, day) = normalize_ymd(year, month, day)
    return datetime.date(year, month, day)


def first_weekday(year, month, weekday):
    (year, month, day) = normalize_ymd(year, month, 1)
    datum = date(year, month, 1)
    difference = datum.isoweekday() - weekday
    if difference < 0:
        difference += 7
    return datum + datetime.timedelta(difference)


EERSTE = 0
TWEEDE = 1
DERDE = 2
VIERDE = 3
VIJFDE = 4
LAATSTE = -1
VOORLAATSTE = -2
VOORVOORLAATSTE = -3

MAANDAG = 1
DINSDAG = 2
WOENSDAG = 3
DONDERDAG = 4
VRIJDAG = 5
ZATERDAG = 6
ZONDAG = 7


# inspired on https://code.activestate.com/recipes/425607-findng-the-xth-day-in-a-month/
def dow_date_finder(year, month, weekday_in_month=EERSTE, day=MAANDAG):
    dow_lst = []

    first_week = list(map(lambda d: datetime.date(year, month, d).isoweekday(), range(1, 8)))
    first_day = first_week.index(day) + 1

    dt = datetime.date(year, month, first_day)
    while dt.month == month:
        dow_lst.append(dt)
        dt = dt + datetime.timedelta(days=7)

    return dow_lst[weekday_in_month]  # may raise an exception if slicing is wrong


class HerhalendeActiviteit(models.Model):
    omschrijving = models.CharField(max_length=200)
    startdatum = models.DateField()
    einddatum = models.DateField()
    starttijd = models.TimeField()
    eindtijd = models.TimeField()
    locatie = models.ForeignKey(Locatie, on_delete=models.PROTECT, blank=False, null=False)
    ruimtes = ChainedManyToManyField(
        Ruimte,
        chained_field="locatie",
        chained_model_field="locatie",
        blank=True
    )

    DAG = 'DAG'
    WEEK = 'WEEK'
    MAAND = 'MAAND'
    JAAR = 'JAAR'
    EENHEID_KEUZES = (
        (WEEK, 'weken'),
        (MAAND, 'maanden'),
    )
    eenheid = models.CharField(max_length=5, choices=EENHEID_KEUZES, default=WEEK, verbose_name='',
                               help_text='Kies of je de activiteit wekelijks of maandelijks wil herhalen')
    eens_per = models.PositiveSmallIntegerField()

    DAGEN_VAN_DE_WEEK = (
        (MAANDAG, 'maandag'),
        (DINSDAG, 'dinsdag'),
        (WOENSDAG, 'woensdag'),
        (DONDERDAG, 'donderdag'),
        (VRIJDAG, 'vrijdag'),
        (ZATERDAG, 'zaterdag'),
        (ZONDAG, 'zondag'),
    )
    WEEKDAGEN_VAN_DE_MAAND = (
        (EERSTE, 'eerste'),
        (TWEEDE, 'tweede'),
        (DERDE, 'derde'),
        (VIERDE, 'vierde'),
        (VIJFDE, 'vijfde'),
        (LAATSTE, 'laatste'),
        (VOORLAATSTE, 'voorlaatste'),
        (VOORVOORLAATSTE, 'voorvoorlaatste')
    )
    dag_van_de_week = models.PositiveSmallIntegerField(blank=True, null=True, choices=DAGEN_VAN_DE_WEEK,
                                                       help_text='Op welke weekdag deze activiteit telkens herhaald '
                                                                 'moet worden.')
    dag_van_de_maand = models.PositiveSmallIntegerField(blank=True, null=True,
                                                        validators=[MinValueValidator(1), MaxValueValidator(31)],
                                                        help_text='Alleen invullen bij maandelijkse herhaling, als je'
                                                                  'niet op een bepaalde weekdag wil herhalen, maar '
                                                                  'bijvoorbeeld elke maand op de 3e dag van de maand. '
                                                                  'In dat voorbeeld vul je hier dus 3 in.')
    weekdag_van_de_maand = models.PositiveSmallIntegerField(blank=True, null=True, choices=WEEKDAGEN_VAN_DE_MAAND,
                                                            help_text='Bij maandelijkse herhaling op een bepaalde '
                                                                      'weekdag, vul je hier in hoeveelste weekdag '
                                                                      'van de maand het is. Dus voor bijvoorbeeld de '
                                                                      '"tweede dinsdag van elke maand", vul je hier'
                                                                      '"tweede" in.')

    taak = models.ForeignKey(Taak, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name_plural = "herhalende activiteiten"

    def maak_activiteiten(self):
        self.save()
        if self.eenheid == self.WEEK:
            offset = (7 - (self.startdatum.isoweekday() - self.dag_van_de_week)) % 7
            start = self.startdatum + datetime.timedelta(offset)
            aantal_keer = 1 + (self.einddatum - start).days // (7 * self.eens_per)
            return list(map(
                lambda x: self.maak_activiteit(start + datetime.timedelta(days=x * (7 * self.eens_per))),
                range(aantal_keer)
            ))
        elif self.eenheid == self.MAAND and self.dag_van_de_week:
            self.normalize_month_dates()
            aantal_keer = months_between(self.startdatum, self.einddatum)
            return list(map(
                lambda x: self.maak_activiteit(
                    dow_date_finder(self.startdatum.year, self.startdatum.month + x, EERSTE, self.dag_van_de_week)),
                range(aantal_keer)
            ))
        elif self.eenheid == self.MAAND and self.dag_van_de_maand:
            self.normalize_month_dates()
            aantal_keer = months_between(self.startdatum, self.einddatum)
            return list(map(
                lambda x: self.maak_activiteit(
                    nth_day_of_month(self.startdatum.year, self.startdatum.month + x, self.dag_van_de_maand)
                ),
                range(aantal_keer)
            ))
        else:
            self.maak_activiteit(self.startdatum)

    def maak_activiteit(self, datum):
        logger.debug('maak_activiteit')
        if self.taak:
            omschrijving = '%s %s' % (self.taak.omschrijving, self.omschrijving)
        else:
            omschrijving = self.omschrijving

        activiteit = Activiteit.objects.create(
            omschrijving=omschrijving,
            datum=datum,
            starttijd=self.starttijd,
            eindtijd=self.eindtijd,
            locatie=self.locatie,
            taak=self.taak,
            herhalend=self,
        )
        activiteit.ruimtes.add(*self.ruimtes.all())
        activiteit.save()

    def normalize_month_dates(self):
        (self.startdatum, self.einddatum) = do_normalize_month_dates(self.startdatum, self.einddatum)

    def clean(self):
        if self.einddatum and self.startdatum and self.einddatum < self.startdatum:
            raise ValidationError('Einddatum moet na startdatum zijn.')
        if self.eindtijd and self.starttijd and self.eindtijd < self.starttijd:
            raise ValidationError('Eindtijd moet later zijn dan starttijd.')
        if not self.dag_van_de_week and self.eenheid != self.DAG:
            raise ValidationError('Dag kan niet leeg zijn bij herhaling per week of maand.')
        if self.eenheid == self.MAAND:
            self.normalize_month_dates()

    def __str__(self):
        return 'Herhalend: %s' % self.omschrijving


FEESTDAG = 'FEESTDAG'
SCHOOLVAKANTIE = 'SCHOOLVAKANTIE'
THEMAWEEK = 'THEMAWEEK'


class BijzondereDag(models.Model):
    TYPES = (
        (FEESTDAG, 'feestdag'),
        (SCHOOLVAKANTIE, 'schoolvakantie'),
        (THEMAWEEK, 'themaweek'),
    )

    omschrijving = models.CharField(max_length=200)
    startdatum = models.DateField()
    einddatum = models.DateField(blank=True, null=True,
                                 help_text="Leeg laten als het een enkele bijzondere dag betreft.")
    type = models.CharField(max_length=15, choices=TYPES)

    objects = BijzonderedagManager(None)
    filter_by = StartEindDatumFilterManager(None)

    class Meta:
        verbose_name_plural = "Bijzondere dagen"
        ordering = ('startdatum', 'einddatum',)

    def __str__(self):
        return self.omschrijving


class Schoolvakantie(BijzondereDag):
    objects = BijzonderedagManager(SCHOOLVAKANTIE)
    filter_by = StartEindDatumFilterManager(SCHOOLVAKANTIE)

    def clean(self):
        self.type = SCHOOLVAKANTIE
        super(Schoolvakantie, self).clean()

    class Meta:
        proxy = True


class Feestdag(BijzondereDag):
    objects = BijzonderedagManager(FEESTDAG)
    filter_by = StartEindDatumFilterManager(FEESTDAG)

    def clean(self):
        self.type = FEESTDAG
        super(Feestdag, self).clean()

    class Meta:
        proxy = True
        verbose_name_plural = 'feestdagen'


class Themaweek(BijzondereDag):
    objects = BijzonderedagManager(THEMAWEEK)
    filter_by = StartEindDatumFilterManager(THEMAWEEK)

    def clean(self):
        self.type = THEMAWEEK
        super(Themaweek, self).clean()

    class Meta:
        proxy = True
        verbose_name_plural = 'themaweken'


categorie_validator = RegexValidator(r'^[0-9a-z\-_+ ]*$',
                                     'Alleen kleine letters, cijfers, spaties, -, + en _ toegestaan.')


class Categorie(models.Model):
    omschrijving = LowerCaseCharField(max_length=200, validators=[categorie_validator])

    class Meta:
        verbose_name_plural = "Categorieën"
        ordering = ('omschrijving',)

    def __str__(self):
        return self.omschrijving


class Activiteit(models.Model):
    omschrijving = models.CharField(max_length=200)
    datum = models.DateField()
    hele_dag = models.BooleanField(help_text="Aanvinken voor activiteiten die de hele dag duren of waarvan de start- "
                                             "en eindtijd nog niet bekend zijn.", default=False)
    starttijd = models.TimeField(blank=True, null=True)
    eindtijd = models.TimeField(blank=True, null=True)
    herhalend = models.ForeignKey(HerhalendeActiviteit, on_delete=models.SET_NULL, blank=True, null=True)
    locatie = models.ForeignKey(Locatie, on_delete=models.PROTECT, blank=True, null=True)
    ruimtes = ChainedManyToManyField(
        Ruimte,
        chained_field="locatie",
        chained_model_field="locatie",
        blank=True
    )
    taak = models.ForeignKey(Taak, on_delete=models.SET_NULL, blank=True, null=True)
    personen = ChainedManyToManyField(
        Persoon,
        chained_field="taak",
        chained_model_field="taken",
        blank=True
    )
    opmerkingen = models.CharField(max_length=500, blank=True, null=True)
    categorieen = models.ManyToManyField(Categorie, verbose_name="Categorieën",
                                         help_text="Begin te typen en selecteer een categorie uit het lijstje." +
                                         "Categorieën worden gebruikt om activiteiten makkelijker te kunnen filteren.",
                                         blank=True, null=True)

    objects = models.Manager()
    filter_by = DatumFilterManager()

    class Meta:
        verbose_name_plural = "activiteiten"
        ordering = ('datum', 'starttijd',)

    def weeknummer(self):
        return self.datum.isocalendar()[1]

    weeknummer.short_description = 'week'

    def jaar(self):
        return self.datum.year

    def waar(self):
        ruimtes_joined = ", ".join([r.naam for r in self.ruimtes.all()])
        if len(ruimtes_joined) > 0:
            if self.locatie.tonen_bij_ruimte:
                return ruimtes_joined + "; " + self.locatie.naam
            else:
                return ruimtes_joined
        else:
            if self.locatie:
                return self.locatie.naam
            else:
                return ""

    def wie(self):
        return ", ".join([p.naam() for p in self.personen.all()])

    def categorieën(self):
        return ", ".join([c.omschrijving for c in self.categorieen.all()])

    def clean(self):
        if self.hele_dag:
            self.starttijd = None
            self.eindtijd = None
        else:
            if self.starttijd and self.eindtijd and self.eindtijd < self.starttijd:
                raise ValidationError('Eindtijd moet later zijn dan starttijd.')

    def __str__(self):
        return self.omschrijving
