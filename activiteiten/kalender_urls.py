from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<van_datum>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/(?P<tot_datum>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/(?P<categorieen>(?:[a-z0-9\-_+ ]+/)*)$',
        views.kalender, name='kalender'),

    url(r'^(?P<tot_datum>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/(?P<categorieen>(?:[a-z0-9\-_+ ]+/)*)$',
        views.kalender_vanaf_vandaag, name='kalender_vanaf_vandaag'),

    url(r'^(?P<categorieen>(?:[a-z0-9\-_+ ]+/)*)$',
        views.kalender_komende_maand, name='kalender_komende_maand'),
]
