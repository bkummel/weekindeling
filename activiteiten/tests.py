# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from datetime import date

from django.core.exceptions import ValidationError
from django.test import TestCase

from activiteiten.models import months_between, do_normalize_month_dates, nth_day_of_month, first_weekday, \
    normalize_ymd, EERSTE, MAANDAG, dow_date_finder, TWEEDE, WOENSDAG, LAATSTE, ZATERDAG, VOORLAATSTE, VRIJDAG, DERDE, \
    ZONDAG, DINSDAG, iso_to_gregorian
from .models import HerhalendeActiviteit


# Create your tests here.

class HerhalendeActiviteitTest(TestCase):
    maxDiff = None

    def maakActiviteit(self, eenheid, dag):
        return HerhalendeActiviteit(
            omschrijving="a",
            startdatum=datetime.datetime.now(),
            einddatum=datetime.datetime.now() + datetime.timedelta(days=60),
            starttijd=20,
            eindtijd=21,
            eenheid=eenheid,
            dag_van_de_week=dag)

    def test_dag_niet_leeg(self):
        act = self.maakActiviteit(HerhalendeActiviteit.MAAND, None)

        with self.assertRaises(ValidationError) as error:
            act.clean()

        self.assertEqual(error.exception.message, 'Dag kan niet leeg zijn bij herhaling per week, maand of jaar.')

    def test_dag_wel_leeg(self):
        act = self.maakActiviteit(HerhalendeActiviteit.DAG, None)

        act.clean()

    def wekelijks(self, start, eind, eens_per=1, expected=None):
        if expected is None:
            expected = []
        act = HerhalendeActiviteit(
            omschrijving="h",
            startdatum=start,
            einddatum=eind,
            starttijd=20,
            eindtijd=21,
            eenheid=HerhalendeActiviteit.WEEK,
            eens_per=eens_per,
            dag_van_de_week=3  # woensdag
        )

        result = act.maak_activiteiten()
        datums = list(map(lambda a: a.datum, result))

        self.assertEquals(datums, expected)

    def test_maak_activiteiten_wekelijks1(self):
        self.wekelijks(
            start=date(2017, 7, 5),  # woensdag
            eind=date(2017, 8, 8),  # dinsdag
            expected=[date(2017, 7, 5), date(2017, 7, 12), date(2017, 7, 19), date(2017, 7, 26), date(2017, 8, 2)]
        )

    def test_maak_activiteiten_wekelijks2(self):
        self.wekelijks(
            start=date(2017, 7, 6),  # donderdag
            eind=date(2017, 8, 9),  # woensdag
            expected=[date(2017, 7, 12), date(2017, 7, 19), date(2017, 7, 26), date(2017, 8, 2), date(2017, 8, 9)]
        )

    def test_maak_activiteiten_wekelijks3(self):
        self.wekelijks(
            start=date(2017, 7, 4),  # dinsdag
            eind=date(2017, 8, 10),  # donderdag
            expected=[date(2017, 7, 5), date(2017, 7, 12), date(2017, 7, 19), date(2017, 7, 26), date(2017, 8, 2),
                      date(2017, 8, 9)]
        )

    def test_maak_activiteiten_wekelijks4(self):
        self.wekelijks(
            start=date(2017, 7, 3),  # maandag
            eind=date(2017, 7, 4),  # dinsdag
            expected=[]
        )

    def test_maak_activiteiten_wekelijks5(self):
        self.wekelijks(
            start=date(2017, 7, 4),  # dinsdag
            eind=date(2017, 7, 5),  # woensdag
            expected=[date(2017, 7, 5)]
        )

    def test_maak_activiteiten_wekelijks6(self):
        self.wekelijks(
            start=date(2017, 7, 4),  # dinsdag
            eind=date(2017, 8, 10),  # donderdag
            eens_per=2,
            expected=[date(2017, 7, 5), date(2017, 7, 19), date(2017, 8, 2)]
        )

    def test_months_between_1(self):
        act = HerhalendeActiviteit()
        self.assertEquals(months_between(date(2017, 3, 12), date(2017, 7, 4)), 5)

    def test_months_between_2(self):
        act = HerhalendeActiviteit()
        self.assertEquals(months_between(date(2017, 3, 12), date(2018, 7, 4)), 17)

    def test_months_between_3(self):
        act = HerhalendeActiviteit()
        self.assertEquals(months_between(date(2017, 3, 12), date(2019, 7, 4)), 29)

    def test_normalize_month_dates_1(self):
        result = do_normalize_month_dates(date(2017, 3, 3), date(2017, 8, 3))
        expected = (date(2017, 3, 1), date(2017, 8, 31))
        self.assertEquals(result, expected)

    def test_normalize_month_dates_2(self):
        result = do_normalize_month_dates(date(2016, 1, 31), date(2016, 2, 3))
        expected = (date(2016, 1, 1), date(2016, 2, 29))
        self.assertEquals(result, expected)

    def test_normalize_month_dates_3(self):
        result = do_normalize_month_dates(date(2015, 1, 20), date(2015, 2, 3))
        expected = (date(2015, 1, 1), date(2015, 2, 28))
        self.assertEquals(result, expected)

    def test_nth_day_of_month_1(self):
        result = nth_day_of_month(2017, 2, 30)
        self.assertEquals(result, date(2017, 2, 28))

    def test_nth_day_of_month_2(self):
        result = nth_day_of_month(2017, 4, 31)
        self.assertEquals(result, date(2017, 4, 30))

    def test_nth_day_of_month_3(self):
        result = nth_day_of_month(2016, 2, 30)
        self.assertEquals(result, date(2016, 2, 29))

    def test_nth_day_of_month_4(self):
        result = nth_day_of_month(2016, 13, 30)
        self.assertEquals(result, date(2017, 1, 30))

    def test_nth_day_of_month_5(self):
        result = nth_day_of_month(2016, 14, 35)
        self.assertEquals(result, date(2017, 2, 28))

    def first_weekday_1(self):
        result = first_weekday(2017, 9, 1)
        self.assertEquals(result, date(2017, 9, 4))

    def first_weekday_2(self):
        result = first_weekday(2017, 10, 7)
        self.assertEquals(result, date(2017, 10, 1))

    def first_weekday_3(self):
        result = first_weekday(2017, 9, 7)
        self.assertEquals(result, date(2017, 9, 3))

    def first_weekday_4(self):
        result = first_weekday(2017, 13, 7)
        self.assertEquals(result, date(2018, 1, 7))

    def test_normalize_ymd_1(self):
        result = normalize_ymd(2017, 13, 31)
        self.assertEquals(result, (2018, 1, 31))

    def test_normalize_ymd_2(self):
        result = normalize_ymd(2017, 25, 31)
        self.assertEquals(result, (2019, 1, 31))

    def test_normalize_ymd_3(self):
        result = normalize_ymd(2017, 12, 31)
        self.assertEquals(result, (2017, 12, 31))

    def test_normalize_ymd_4(self):
        result = normalize_ymd(2017, 2, 31)
        self.assertEquals(result, (2017, 2, 28))

    def test_normalize_ymd_5(self):
        result = normalize_ymd(2015, 14, 31)
        self.assertEquals(result, (2016, 2, 29))

    def test_normalize_ymd_6(self):
        result = normalize_ymd(2018, 8, 31)
        self.assertEquals(result, (2018, 8, 31))

    def test_normalize_ymd_7(self):
        result = normalize_ymd(2018, 9, 31)
        self.assertEquals(result, (2018, 9, 30))

    def maandelijks(self, start, eind, dag_van_de_week=None, dag_van_de_maand=None, expected=None):
        if expected is None:
            expected = []
        act = HerhalendeActiviteit(
            omschrijving="maandelijks",
            startdatum=start,
            einddatum=eind,
            starttijd=20,
            eindtijd=21,
            eenheid=HerhalendeActiviteit.MAAND,
            eens_per=1,
            dag_van_de_week=dag_van_de_week,
            dag_van_de_maand=dag_van_de_maand,
        )

        result = act.maak_activiteiten()
        datums = list(map(lambda a: a.datum, result))

        self.assertEquals(expected, datums)

    def test_maak_activiteiten_maandelijks_1(self):
        self.maandelijks(
            start=date(2017, 1, 1),
            eind=date(2017, 12, 31),
            dag_van_de_week=2,
            expected=[
                date(2017, 1, 3),
                date(2017, 2, 7),
                date(2017, 3, 7),
                date(2017, 4, 4),
                date(2017, 5, 2),
                date(2017, 6, 6),
                date(2017, 7, 4),
                date(2017, 8, 1),
                date(2017, 9, 5),
                date(2017, 10, 3),
                date(2017, 11, 7),
                date(2017, 12, 5),
            ]
        )

    def test_dow_date_finder_1(self):
        result = dow_date_finder(2017, 10, EERSTE, MAANDAG)
        expected = date(2017, 10, 2)
        self.assertEquals(result, expected)

    def test_dow_date_finder_2(self):
        result = dow_date_finder(2017, 10, TWEEDE, DINSDAG)
        expected = date(2017, 10, 10)
        self.assertEquals(result, expected)

    def test_dow_date_finder_3(self):
        result = dow_date_finder(2017, 10, EERSTE, WOENSDAG)
        expected = date(2017, 10, 4)
        self.assertEquals(result, expected)

    def test_dow_date_finder_4(self):
        result = dow_date_finder(2017, 10, LAATSTE, ZATERDAG)
        expected = date(2017, 10, 28)
        self.assertEquals(result, expected)

    def test_dow_date_finder_5(self):
        result = dow_date_finder(2017, 10, VOORLAATSTE, ZONDAG)
        expected = date(2017, 10, 22)
        self.assertEquals(result, expected)

    def test_dow_date_finder(self):
        result = dow_date_finder(2017, 10, DERDE, VRIJDAG)
        expected = date(2017, 10, 20)
        self.assertEquals(result, expected)

    def test_iso_to_gregorian_1(self):
        result = iso_to_gregorian(2017, 45)
        expected = date(2017, 11, 6)
        self.assertEquals(result, expected)

    def test_iso_to_gregorian_2(self):
        result = iso_to_gregorian(2017, 1)
        expected = date(2017, 1, 2)
        self.assertEquals(result, expected)

