# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from datetime import timedelta
from enum import Enum

from django.db import models
from django.db.models import Q, CharField


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


def iso_to_gregorian(iso_year, iso_week, iso_day=1):
    """Gregorian calendar date for the given ISO year, week and day"""
    fourth_jan = datetime.date(iso_year, 1, 4)
    _, fourth_jan_week, fourth_jan_day = fourth_jan.isocalendar()
    return fourth_jan + datetime.timedelta(days=iso_day-fourth_jan_day, weeks=iso_week-fourth_jan_week)


class DatumFilterManager(models.Manager):
    def weeknummer(self, year, week_number):
        start_date = iso_to_gregorian(int(year), int(week_number))
        end_date = start_date + timedelta(days=6)
        return super(DatumFilterManager, self).get_queryset().filter(datum__range=[start_date, end_date])

    def datumbereik(self, datum_van, datum_tot):
        return super(DatumFilterManager, self).get_queryset().filter(datum__range=[datum_van, datum_tot])


class BijzonderedagManager(models.Manager):
    def __init__(self, type):
        super().__init__()
        self.type = type

    def create(self, **kwargs):
        kwargs.update({'type': self.type})
        return super(BijzonderedagManager, self).create(**kwargs)

    def get_queryset(self):
        if self.type:
            return super(BijzonderedagManager, self).get_queryset().filter(type=self.type)
        else:
            return super(BijzonderedagManager, self).get_queryset()

    pass


class StartEindDatumFilterManager(BijzonderedagManager):
    def weeknummer(self, year, week_number):
        start_date = iso_to_gregorian(int(year), int(week_number))
        end_date = start_date + timedelta(days=6)
        return super(StartEindDatumFilterManager, self).get_queryset().filter(
            Q(Q(einddatum=None) | Q(einddatum__gte=start_date)) & Q(startdatum__lte=end_date)
        )

    def datumbereik(self, datum_van, datum_tot):
        return super(StartEindDatumFilterManager, self).get_queryset().filter(
            Q(Q(einddatum=None) | Q(einddatum__gte=datum_van)) & Q(startdatum__lte=datum_tot)
        )


class ModifyingFieldDescriptor(object):
    """ Modifies a field when set using the field's (overriden) .to_python() method. """
    def __init__(self, field):
        self.field = field

    def __get__(self, instance, owner=None):
        if instance is None:
            raise AttributeError('Can only be accessed via an instance.')
        return instance.__dict__[self.field.name]

    def __set__(self, instance, value):
        instance.__dict__[self.field.name] = self.field.to_python(value)


class LowerCaseCharField(CharField):
    def to_python(self, value):
        value = super(LowerCaseCharField, self).to_python(value)
        if isinstance(value, str):
            return value.lower()
        return value

    def contribute_to_class(self, cls, name, **kwargs):
        super(LowerCaseCharField, self).contribute_to_class(cls, name)
        setattr(cls, self.name, ModifyingFieldDescriptor(self))
