(function($) {
    $(document).ready(function() {
        toggle_time_fields = function(visible) {
            $(".field-starttijd").toggle(visible)
            $(".field-eindtijd").toggle(visible)
        }
        toggle_required = function(field, required) {
            if (required) {
                field.addClass("required")
            } else {
                field.removeClass("required")
            }
        }

        $("label[for='id_starttijd']").addClass("required");
        $("label[for='id_eindtijd']").addClass("required");
        toggle_time_fields(!$("#id_hele_dag").is(":checked"))
        toggle_required($("label[for='id_locatie']"), !$("#id_hele_dag").is(":checked"))

        $("#id_hele_dag").change(function(e){
            toggle_time_fields(!e.target.checked)
            toggle_required($("label[for='id_locatie']"), !e.target.checked)
        })
    });
})(django.jQuery);


