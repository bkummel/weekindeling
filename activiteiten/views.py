# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from datetime import timedelta

from dateutil import relativedelta
from dateutil.parser import parse
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.template import loader

from activiteiten.model_utils import iso_to_gregorian
from activiteiten.models import Activiteit, BijzondereDag, Categorie
from taken.models import TaakUitvoering, Taak


def weekoverzicht_deze_week(request):
    vandaag = datetime.datetime.now()
    jaar, weeknummer, _ = vandaag.isocalendar()
    return weekoverzicht(request, jaar, weeknummer)


@login_required
def weekoverzicht(request, jaar, weeknummer):
    activiteiten = Activiteit.filter_by.weeknummer(year=jaar, week_number=weeknummer)
    taak_uitvoeringen = TaakUitvoering.filter_by.weeknummer(year=jaar, week_number=weeknummer)
    bijzondere_dagen = BijzondereDag.filter_by.weeknummer(year=jaar, week_number=weeknummer)
    template = loader.get_template('activiteiten/weekoverzicht.html')
    van_datum = iso_to_gregorian(int(jaar), int(weeknummer))
    tot_datum = van_datum + timedelta(days=6)
    context = {
        'activiteiten': activiteiten,
        'taak_uitvoeringen': taak_uitvoeringen,
        'bijzondere_dagen': bijzondere_dagen,
        'weeknummer': weeknummer,
        'jaar': jaar,
        'vorige': vorige(jaar, weeknummer),
        'volgende': volgende(jaar, weeknummer),
        'van_datum': van_datum,
        'tot_datum': tot_datum,
    }
    return HttpResponse(template.render(context, request))


def vorige(jaar, weeknummer):
    datum = iso_to_gregorian(int(jaar), int(weeknummer)) - timedelta(days=7)
    vorig_jaar, vorige_week, _ = datum.isocalendar()
    return {
        'jaar': vorig_jaar,
        'weeknummer': vorige_week
    }


def volgende(jaar, weeknummer):
    datum = iso_to_gregorian(int(jaar), int(weeknummer)) + timedelta(days=7)
    volgend_jaar, volgende_week, _ = datum.isocalendar()
    return {
        'jaar': volgend_jaar,
        'weeknummer': volgende_week
    }


@login_required
def kalender_komende_maand(request, categorieen):
    return kalender(request, datetime.date.today(), datetime.date.today() + relativedelta.relativedelta(months=1), categorieen)


@login_required
def kalender_vanaf_vandaag(request, tot_datum, categorieen):
    return kalender(request, datetime.date.today(), tot_datum, categorieen)


@login_required
def kalender(request, van_datum, tot_datum, categorieen):
    if not tot_datum:
        tot_datum = van_datum + relativedelta.relativedelta(months=1)

    if not type(van_datum) in (datetime, datetime.datetime, datetime.date):
        van_datum = parse(van_datum)
    if not type(tot_datum) in (datetime, datetime.datetime, datetime.date):
        tot_datum = parse(tot_datum)

    categorieen_list = categorieen.split('/')[:-1]
    categorieen_object_list = Categorie.objects.filter(omschrijving__in=categorieen_list).distinct()
    activiteiten = Activiteit.filter_by.datumbereik(van_datum, tot_datum)
    if len(categorieen_list) > 0:
        activiteiten = activiteiten.filter(categorieen__omschrijving__in=categorieen_list).distinct()
    bijzondere_dagen = BijzondereDag.filter_by.datumbereik(van_datum, tot_datum)
    template = loader.get_template('activiteiten/kalender.html')
    context = {
        'activiteiten': activiteiten,
        'bijzondere_dagen': bijzondere_dagen,
        'van_datum': van_datum,
        'tot_datum': tot_datum,
        'categorieen': categorieen_object_list,
    }
    return HttpResponse(template.render(context, request))


def alle_taken(request):
    return {
        'taken': Taak.objects.filter(indeling_tonen=True),
    }


@login_required
def taak_overzicht_default(request, slug):
    today = datetime.date.today()
    end_of_year = datetime.date(today.year, 12, 31)
    return taak_overzicht(request, slug, today, end_of_year)


@login_required
def taak_overzicht(request, slug, van_datum, tot_datum):
    if not tot_datum:
        tot_datum = van_datum + relativedelta.relativedelta(months=1)

    if not type(van_datum) in (datetime, datetime.datetime, datetime.date):
        van_datum = parse(van_datum)
    if not type(tot_datum) in (datetime, datetime.datetime, datetime.date):
        tot_datum = parse(tot_datum)

    taak = next(filter(lambda t: t.slug() == slug, Taak.objects.all()), None)
    activiteiten = Activiteit.objects\
        .filter(taak=Taak.objects.filter(omschrijving=taak.omschrijving)[0]) \
        .filter(datum__range=[van_datum, tot_datum])
    taak_uitvoeringen = TaakUitvoering.objects\
        .filter(taak=Taak.objects.filter(omschrijving=taak.omschrijving)[0]) \
        .filter(datum__range=[van_datum, tot_datum])
    bijzondere_dagen = BijzondereDag.filter_by.datumbereik(van_datum, tot_datum)
    template = loader.get_template('activiteiten/taak.html')
    context = {
        'taak': taak,
        'activiteiten': activiteiten,
        'taak_uitvoeringen': taak_uitvoeringen,
        'bijzondere_dagen': bijzondere_dagen,
        'van_datum': van_datum,
        'tot_datum': tot_datum,
    }
    return HttpResponse(template.render(context, request))
