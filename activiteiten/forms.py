
from dal import autocomplete

from activiteiten.models import Activiteit, HerhalendeActiviteit
from taken.fixed_model_form import FixedModelForm


class ActiviteitenForm(FixedModelForm):
    class Meta:
        model = Activiteit
        fields = ('__all__')
        widgets = {
            'ruimtes': autocomplete.ModelSelect2Multiple(url='ruimte-autocomplete', forward=['locatie']),
            'personen': autocomplete.ModelSelect2Multiple(url='persoon-autocomplete', forward=['taak'])
        }
        help_texts = {
            'ruimtes': 'Begin te typen en selecteer een of meer ruimtes uit het lijstje.',
            'personen': 'Begin te typen en selecteer een of meer personen uit het lijstje.',
        }

    class Media:
        js = ("activiteiten/js/clear_dependants_activiteiten_form.js","activiteiten/js/activiteiten_form.js")

    def clean(self):
        cleaned_data = super(ActiviteitenForm, self).clean()
        hele_dag = cleaned_data.get("hele_dag")
        starttijd = cleaned_data.get("starttijd")
        eindtijd = cleaned_data.get("eindtijd")
        locatie = cleaned_data.get("locatie")
        if not hele_dag:
            if not locatie:
                self.add_error('locatie', 'Dit veld is verplicht.')
            if not starttijd:
                self.add_error('starttijd', 'Dit veld is verplicht.')
            if not eindtijd:
                self.add_error('eindtijd', 'Dit veld is verplicht.')



class HerhalendeActiviteitenForm(FixedModelForm):
    class Meta:
        model = HerhalendeActiviteit
        fields = ('__all__')
        widgets = {
            'ruimtes': autocomplete.ModelSelect2Multiple(url='ruimte-autocomplete', forward=['locatie']),
        }
        help_texts = {
            'ruimtes': 'Begin te typen en selecteer een of meer ruimtes uit het lijstje.',
        }

    class Media:
        js = ("activiteiten/js/clear_dependants_activiteiten_form.js",)


class ActiviteitDetailInlineForm(FixedModelForm):
    class Meta:
        model = Activiteit
        fields = ('__all__')
        widgets = {
            'personen': autocomplete.ModelSelect2Multiple(url='persoon-autocomplete', forward=['taak'])
        }
        help_texts = {
            'personen': 'Begin te typen en selecteer een of meer personen uit het lijstje.'
        }
