from django.conf.urls import url

from . import views

urlpatterns = [
    # ex: /weekoverzicht/
    url(r'^$', views.weekoverzicht_deze_week, name='weekoverzicht_deze_week'),
    # ex: /weekoverzicht/2017/48
    url(r'^(?P<jaar>[0-9]{4})/(?P<weeknummer>[0-9]{1,2})/$', views.weekoverzicht, name='weekoverzicht'),
]