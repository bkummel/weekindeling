from django.conf.urls import url

from activiteiten.autocomplete_views import RuimteAutocomplete, CategorieenAutocomplete
from .autocomplete_views import LocatieAutocomplete

urlpatterns = [
    url(
        r'^locatie/$',
        LocatieAutocomplete.as_view(),
        name='locatie-autocomplete',
    ),
    url(
        r'^ruimte/$',
        RuimteAutocomplete.as_view(),
        name='ruimte-autocomplete',
    ),
    url(
        r'^categorieen/$',
        CategorieenAutocomplete.as_view(),
        name='categorie-autocomplete',
    ),
]
