from django import template

register = template.Library()


@register.filter(name='filterbydate')
def filterbydate(value, arg):
    return [x for x in value if x.datum == arg]


@register.filter(name='filterholidays')
def filterholidays(value, arg):
    return [x for x in value if
            (x.einddatum and x.startdatum <= arg <= x.einddatum) or
            (x.startdatum == arg and x.einddatum is None)]


@register.filter(name='min')
def min_value(value, arg):
    return min(value, arg)


@register.filter(name='equals')
def equals(value, arg):
    return value == arg


@register.filter(name='add_class')
def add_class(field, css):
    return field.as_widget(attrs={"class": css})
