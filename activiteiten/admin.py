# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.contrib.admin.utils import (
    quote,
)
from django import forms
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.http import urlquote
from django.utils.translation import ugettext as _

from activiteiten.forms import ActiviteitenForm, HerhalendeActiviteitenForm, ActiviteitDetailInlineForm
from .models import Activiteit, HerhalendeActiviteit, Ruimte, Locatie, Verwarmingsgroep, BijzondereDag


class MyModelAdmin(admin.ModelAdmin):
    grouping = ""


class ButtonableModelAdmin(MyModelAdmin):
    """
    A subclass of this admin will let you add buttons (like history) in the
    change view of an entry.

    ex.
    class FooAdmin(ButtonableModelAdmin):
      ...

      def bar(self, obj):
         obj.bar()
      bar.short_description='Example button'

      buttons = [ bar ]

    you can then put the following in your admin/change_form.html template:

      {% block object-tools %}
      {% if change %}{% if not is_popup %}
      <ul class="object-tools">
      {% for button in buttons %}
         <li><a href="{{ button.func_name }}/">{{ button.short_description }}</a></li>
      {% endfor %}
      <li><a href="history/" class="historylink">History</a></li>
      {% if has_absolute_url %}<li><a href="../../../r/{{ content_type_id }}/{{ object_id }}/" class="viewsitelink">View on site</a></li>{% endif%}
      </ul>
      {% endif %}{% endif %}
      {% endblock %}

    """
    buttons = []

    def btn(self, button_function):
        return {'name': button_function.__name__, 'desc': button_function.short_description}

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if extra_context is None:
            extra_context = {}
        extra_context['buttons'] = map(self.btn, self.buttons)
        return super(ButtonableModelAdmin, self).change_view(request, object_id, form_url, extra_context)

    def _changeform_view(self, request, object_id, form_url, extra_context):
        for btn in self.buttons:
            if request.method == 'POST' and "_" + btn.__name__ in request.POST:
                obj = self.model._default_manager.get(pk=object_id)
                getattr(self, btn.__name__)(obj)

                opts = obj._meta
                pk_value = obj._get_pk_val()
                preserved_filters = self.get_preserved_filters(request)
                obj_url = reverse(
                    'admin:%s_%s_change' % (opts.app_label, opts.model_name),
                    args=(quote(pk_value),),
                    current_app=self.admin_site.name,
                )
                msg_dict = {
                    'name': force_text(opts.verbose_name),
                    'obj': format_html('<a href="{}">{}</a>', urlquote(request.path), obj),
                }
                msg = format_html(
                    _('The {name} "{obj}" was added successfully. You may edit it again below.'),
                    **msg_dict
                )
                self.message_user(request, msg, messages.SUCCESS)
                post_url_continue = obj_url
                post_url_continue = add_preserved_filters(
                    {'preserved_filters': preserved_filters, 'opts': opts},
                    post_url_continue
                )
                return HttpResponseRedirect(post_url_continue)

        return super(ButtonableModelAdmin, self)._changeform_view(request, object_id, form_url, extra_context)


class JaarListFilter(admin.SimpleListFilter):
    title = 'jaar'
    parameter_name = 'jaar'

    def lookups(self, request, model_admin):
        ditjaar = datetime.now().year
        return [(ditjaar - 1, ditjaar - 1), (ditjaar, ditjaar), (ditjaar + 1, ditjaar + 1)]

    def queryset(self, request, queryset):
        if self.value():
            first_day = datetime.strptime('{}-01-01'.format(self.value()), '%Y-%m-%d')
            last_day = datetime.strptime('{}-12-31'.format(self.value()), '%Y-%m-%d')
            return queryset.filter(datum__gte=first_day, datum__lte=last_day)
        else:
            return queryset


class WeeknummerListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'weeknummer'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'weeknummer'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        return map(lambda x: (x, x), range(1, 53))

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        related_filter_parameter = 'jaar'

        if related_filter_parameter in request.GET:
            jaar = request.GET[related_filter_parameter]
        else:
            jaar = ditjaar = datetime.now().year

        if self.value():
            first_day = datetime.strptime('{}w{}-1'.format(jaar, self.value().zfill(2)), '%Yw%W-%w')
            last_day = datetime.strptime('{}w{}-6'.format(jaar, self.value().zfill(2)), '%Yw%W-%w')
            return queryset.filter(datum__gte=first_day, datum__lte=last_day)
        else:
            return queryset


class BijzondereDagAdmin(MyModelAdmin):
    list_display = ('omschrijving', 'startdatum', 'einddatum', 'type')
    date_hierarchy = 'startdatum'
    list_display_links = ['omschrijving']
    ordering = ['startdatum', 'einddatum']
    fields = ('omschrijving', 'startdatum', 'einddatum', 'type')
    grouping = 'feestdagen en vakanties'
    search_fields = ['omschrijving']


class FeestdagForm(forms.ModelForm):
    class Meta:
        fields = ['omschrijving', 'startdatum']
        labels = {
            'omschrijving': 'Omschrijving',
            'startdatum': 'Datum',
        }


class FeestdagAdmin(MyModelAdmin):
    list_display = ('omschrijving', 'startdatum')
    date_hierarchy = 'startdatum'
    list_display_links = ['omschrijving']
    ordering = ['startdatum']
    fields = ('omschrijving', 'startdatum')
    form = FeestdagForm
    grouping = 'feestdagen en vakanties'
    search_fields = ['omschrijving']


class SchoolvakantieAdmin(MyModelAdmin):
    list_display = ('omschrijving', 'startdatum', 'einddatum')
    date_hierarchy = 'startdatum'
    list_display_links = ['omschrijving']
    ordering = ['startdatum', 'einddatum']
    fields = ('omschrijving', 'startdatum', 'einddatum')
    grouping = 'feestdagen en vakanties'
    search_fields = ['omschrijving']


class ThemaweekAdmin(MyModelAdmin):
    list_display = ('omschrijving', 'startdatum', 'einddatum')
    date_hierarchy = 'startdatum'
    list_display_links = ['omschrijving']
    ordering = ['startdatum', 'einddatum']
    fields = ('omschrijving', 'startdatum', 'einddatum')
    grouping = 'feestdagen en vakanties'
    search_fields = ['omschrijving']


class CategorieAdmin(MyModelAdmin):
    search_fields = ['omschrijving']


class ActiviteitAdmin(MyModelAdmin):
    list_display = ('taak', 'omschrijving', 'wie', 'weeknummer', 'datum', 'starttijd', 'eindtijd', 'waar',
                    'categorieën')
    date_hierarchy = 'datum'
    list_display_links = ['omschrijving']
    list_filter = ('categorieen', 'taak', 'locatie', 'personen', JaarListFilter, WeeknummerListFilter)
    ordering = ['datum']
    preserve_filters = True
    raw_id_fields = ('herhalend',)
    form = ActiviteitenForm
    search_fields = ['omschrijving', 'ruimtes__naam', 'taak__omschrijving', 'categorieen__omschrijving']
    autocomplete_fields = ['categorieen']


class ActiviteitDetailInline(admin.TabularInline):
    model = Activiteit
    fields = ('omschrijving', 'personen', 'datum', 'starttijd', 'eindtijd', 'opmerkingen', 'categorieen')
    readonly_fields = ('locatie', 'ruimtes', 'taak')
    form = ActiviteitDetailInlineForm
    autocomplete_fields = ['categorieen']


class HerhalendeActiviteitAdmin(ButtonableModelAdmin):
    list_display = ('taak', 'omschrijving', 'startdatum', 'einddatum', 'eens_per', 'eenheid')
    date_hierarchy = 'startdatum'
    list_display_links = ['omschrijving']
    ordering = ['startdatum']
    preserve_filters = True
    inlines = [ActiviteitDetailInline, ]
    form = HerhalendeActiviteitenForm
    fieldsets = (
        (None, {
            'description': 'Hier kan je een herhalende activiteit invoeren. Als je alles hebt ingevuld, klik je op '
                           '"Opslaan en opnieuw bewerken". Daarna klik je op "Maak activiteiten" om de activiteiten '
                           'te maken volgens het herhalingspatroon dat je hebt opgegeven.',
            'fields': (
                'taak',
                'omschrijving',
                ('startdatum', 'einddatum'),
                ('starttijd', 'eindtijd'),
                'locatie',
                'ruimtes',
            ),
        }),
        ('Herhaling', {
            'description': 'Hier vul je in volgens welk patroon je de activiteit wil herhalen.',
            'fields': (
                ('eens_per', 'eenheid'),
                'dag_van_de_week',
                'dag_van_de_maand',
                'weekdag_van_de_maand',
            )
        }),
    )
    search_fields = ['omschrijving', 'ruimtes__naam', 'taak__omschrijving']

    def get_formsets_with_inlines(self, request, obj=None):
        """By overriding this function, we achieve that the inline detail view is only shown in 'edit' mode, not in
           'add' mode."""
        if obj:  # obj will be None in 'add' mode
            return super(ButtonableModelAdmin, self).get_formsets_with_inlines(request, obj)
        else:
            return []

    def maak_activiteiten_button(self, obj):
        obj.save()
        obj.maak_activiteiten()

    maak_activiteiten_button.short_description = 'Maak activiteiten'

    buttons = [maak_activiteiten_button]


class RuimteAdmin(MyModelAdmin):
    list_display = ('naam', 'locatie', 'verwarmingsgroep', 'capaciteit', 'opmerkingen')
    ordering = ['locatie', 'naam']
    search_fields = ['naam', 'locatie__naam', 'verwarmingsgroep__naam']


class RuimteDetailInline(admin.TabularInline):
    model = Ruimte


class LocatieAdmin(MyModelAdmin):
    list_display = ('naam', )
    ordering = ['naam']
    inlines = [RuimteDetailInline, ]
    search_fields = ['naam']


class VerwarmingsgroepAdmin(MyModelAdmin):
    list_display = ('naam', 'locatie')
    ordering = ['naam']
    inlines = [RuimteDetailInline, ]
    search_fields = ['naam']
