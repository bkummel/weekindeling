from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<slug>[A-Za-z0-9-_]*)/'
        r'(?P<van_datum>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/(?P<tot_datum>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/$',
        views.taak_overzicht, name='taak'),

    url(r'^(?P<slug>[A-Za-z0-9-_]*)/$',
        views.taak_overzicht_default, name='taak-default'),

]
