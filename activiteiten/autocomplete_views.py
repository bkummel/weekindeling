from dal import autocomplete

from activiteiten.models import Locatie, Ruimte, Categorie


class RuimteAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Ruimte.objects.none()

        qs = Ruimte.objects.all()

        locatie = self.forwarded.get('locatie', None)

        if locatie:
            qs = qs.filter(locatie=locatie)
        else:
            return Ruimte.objects.none()

        if self.q:
            qs = qs.filter(naam__istartswith=self.q)

        return qs


class LocatieAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Locatie.objects.none()

        qs = Locatie.objects.all()

        if self.q:
            qs = qs.filter(naam__istartswith=self.q)

        return qs


class CategorieenAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Categorie.objects.none()

        qs = Categorie.objects.all()

        if self.q:
            qs = qs.filter(omschrijving__istartswith=self.q)

        return qs
