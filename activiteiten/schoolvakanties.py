import requests
import json


def get_schoolvakanties(schooljaar):
    r = requests.get('https://opendata.rijksoverheid.nl/v1/sources/rijksoverheid/infotypes/schoolholidays/schoolyear/%s?output=json' % schooljaar)

    jsoncontent = r.json()
    vakanties = jsoncontent['content']['vacations']
    for vakantie in vakanties:
        print(vakantie['type'])
